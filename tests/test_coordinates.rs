use mcai_types::Coordinates;

#[test]
pub fn test_coordinates() {
  let coordinates = Coordinates::new(123, -45);

  assert_eq!(coordinates, Coordinates { x: 123, y: -45 });
}
