use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
pub struct Coordinates {
  pub x: isize,
  pub y: isize,
}

impl Coordinates {
  pub fn new(x: isize, y: isize) -> Self {
    Self { x, y }
  }
}
